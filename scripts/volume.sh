#!/bin/sh
# Initially from <https://github.com/AmadeusWM/dotfiles-hyprland>

# Get Volume
get_volume() {
	status=`pamixer --get-mute`

	if [[ "$status" == "false" ]]; then
		volume=`pamixer --get-volume`
		echo "$volume"
	else
		echo "Mute"
	fi
}

# Get icons
get_icon() {
	vol="$(get_volume)"
	current="${vol%%%}"
	status=`pamixer --get-mute`

	if [[ "$status" == "false" ]]; then
		if [[ "$current" -eq "0" ]]; then
			echo "ﱝ"
		elif [[ ("$current" -ge "0") && ("$current" -le "30") ]]; then
			echo "奄"
		elif [[ ("$current" -ge "30") && ("$current" -le "60") ]]; then
			echo "奔"
		elif [[ ("$current" -ge "60") && ("$current" -le "100") ]]; then
			echo "墳"
		fi
	else
		echo "ﱝ"
	fi
}

# Increase Volume
inc_volume() {
    pamixer --increase 1
}

# Decrease Volume
dec_volume() {
    pamixer --decrease 1
}

# Toggle Mute
toggle_mute() {
    pamixer --toggle-mute
}

# Execute accordingly
if [[ "$1" == "--get" ]]; then
	get_volume
elif [[ "$1" == "--icon" ]]; then
	get_icon
elif [[ "$1" == "--inc" ]]; then
	inc_volume
elif [[ "$1" == "--dec" ]]; then
	dec_volume
elif [[ "$1" == "--toggle" ]]; then
	toggle_mute
else
	get_volume
fi
