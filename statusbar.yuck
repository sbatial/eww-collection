;; Widgets
(defwidget icon-button [text ?onclick]
  (button :onclick onclick
    :class "rounded"
    (label
      :class "icon"
      :text text
      )
    )
  )

(defwidget powerbutton []
  (box :class "container trans powerbutton rounded"
      :space-evenly false ;; necessary to prevent revealer from adding space when closed
    (revealer
      :transition "slideleft"
      :reveal powermenu_open
      (box
        (icon-button :text "󰐥" :onclick "systemctl poweroff") ;; md-power f0425
        (icon-button :text "󰑐" :onclick "systemctl reboot") ;; md-refresh f0450
        ;; swaylock seems to have a bug rn where I can't unlock so... (icon-button :text "󰍁" :onclick "swaylock") ;; md-lock_outline f0341
        )
      )
      (icon-button :onclick "${EWW_CMD} update powermenu_open=${!powermenu_open}"
                   :text "${powermenu_open ? '󰅖' : '󰐦'}" ;; md-close f0156 and md-power_settings f0426
                   )
    )
  )

(defwidget workspaces []
  (eventbox :onscroll "bash ~/.config/eww/scripts/hypr/change-active-workspace.sh {} ${current_workspace}" :class "workspaces-widget"
    (box :space-evenly true
      (label :text "${workspaces}${current_workspace}" :visible false)
      (for workspace in workspaces
        (eventbox :onclick "hyprctl dispatch workspace ${workspace.id}"
          (box :class "workspace-entry ${workspace.id == current_workspace ? "current" : ""} ${workspace.windows > 0 ? "occupied" : "empty"}"
               :width 16
            (label :text "${workspace.id}")
            )
          )
        )
      )
    )
  )

(defwidget clock []
  (box
    :class "clock container"
      (label :text "${time}")
    )
  )

(defwidget battery [capacity ?status]
  (box
    :class
    "${capacity > 40 ? 'good'
     : capacity > 25 ? 'medium'
     : capacity > 10 ? 'low'
     :                 'critical'} bat container"
      (label
         :text '${status =~ "Charging" ? '' : ''} ${capacity}%'
      )
    )
  )

(defwidget performance []
  (eventbox
    :visible {strlength(performance) > 0}
    (box
      :class "performance container ${performance}"
      :orientation "vertical"
      (label :text "${performance == "Quiet" ? "Q" : performance == "Balanced" ? "B" : "P"}")
    )
  )
)

;; Sections/Structural Widgets
(defwidget left []
  (box
    :class "container"
    :style "padding-left: 5px;"
    :halign "start"
    (workspaces)
    )
  )

(defwidget right []
  (box
  (box
    :spacing 6
    :space-evenly false
    :halign "end"
    (performance)
    (battery :capacity {jq(EWW_BATTERY, ".[\"BAT0\"]?.capacity")}
             :status {jq(EWW_BATTERY, ".[\"BAT0\"]?.status")})
    (clock)
    (powerbutton)
    )
    )
  )

;; Windows
(defwindow bar
  :monitor 0
  :exclusive true
  :geometry (geometry
    :anchor "top center"
    :width "99%"
    :y 1
    )
  (box
    (left)
    (right)
   )
  )
