[![built with nix](https://img.shields.io/badge/-Built%20with%20nix-white?style=flat-square&logo=NixOS)](https://builtwithnix.org)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/sbatial%2Feww-collection?style=flat-square&logo=gitlab&labelColor=white&color=%23B6E2A1)](https://gitlab.com/sbatial/eww-collection/-/commits/trunk/)

# eww collection

Some eww configs I've been experimenting with

# windows

## statusbar

Some of the usual widgets like:
- workspaces (https://hyprland.org)
- clock
- performance mode (https://asus-linux.org)
- battery
- powermenu

## hover-clock

A simple clock which is hidden by default and opens when hovering over a specified area

I'm generally a friend of a barless setup but find myself wanting to know the time quite regularly.\
This is an attempt at a middleground between a permanent bar and a fully barless setup

## notif (WIP)

Notifications
