{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.programs.eww-collection;
  eww-package = pkgs.wayland;
in {
  options.programs.eww-collection = {
    enable = lib.mkEnableOption null // {description = lib.mdDoc '''';};
  };

  config = lib.mkIf cfg.enable {
    programs.eww = {
      inherit (cfg) enable;
      # enable = true;
      package = eww-package;
      configDir = ./.;
    };
  };
}
